import {Component, OnInit} from '@angular/core';
import {Dragon, generateRandomDragon} from '../../model/Dragon';

@Component({
  selector: 'app-dragons',
  template: `<h2>Liste de dragons</h2>
    Le boss des dragons est {{ boss.name }}
  `
})
export class DragonsComponent implements OnInit {
  boss: Dragon;

  constructor() {}

  ngOnInit() {
    this.boss = generateRandomDragon('Voldemort');
  }
}
