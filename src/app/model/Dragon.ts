import { Random } from '../services/Random';

const RANDOM_COLORS = [
    'sapphire',
    'emerald',
    'ruby',
    'topaz',
    'diamond',
    'gold',
    'silver',
    'copper',
    'tin',
    'jade'
];

const RANDOM_POWERS = [
    'fire',
    'ice',
    'wind',
    'earth',
    'gravity',
    'void',
    'water',
    'thunder',
    'arcana'
];

const getRandomSize = (): number => Random.getIntegerBetweenValues(1, 100);

const getRandomColor = (): string => RANDOM_COLORS[Random.getIntegerBetweenValues(0, RANDOM_COLORS.length - 1)];

const getRandomPower = (): string => RANDOM_POWERS[Random.getIntegerBetweenValues(0, RANDOM_POWERS.length - 1)];

export interface Dragon {
    name: string;
    size: number;
    color: string;
    power: string;
    lifePoints: number;
}

export function generateRandomDragon(name: string, lifePoints = 100): Dragon {
    return {
        name: name,
        lifePoints: lifePoints,
        size: getRandomSize(),
        color: getRandomColor(),
        power: getRandomPower()
    };
}